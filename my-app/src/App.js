import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import DropBody from "./DropBody";

const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [
        { id: 1, content: "Introduction To habitat", src: "/pic1.jpg" },
        { id: 2, content: "Modules", src: "/pic2.jpg" },
        { id: 3, content: "Getting Started", src: "/pic3.jpg" }
      ]
    };
    this.onDragEnd = this.onDragEnd.bind(this);
  }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      this.state.items,
      result.source.index,
      result.destination.index
    );

    this.setState({
      items,
    });
  }

  render() {
    let { items } = this.state;
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <DropBody items={items} />
      </DragDropContext>
    );
  }
}

export default App;
