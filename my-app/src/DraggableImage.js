import React, { Component } from 'react';
import { Draggable } from 'react-beautiful-dnd';

const grid = 8;
const getItemStyle = (isDragging, draggableStyle) => ({
    userSelect: 'none',
    padding: grid * 2,
    margin: `0 ${grid}px 0 0`,
    background: isDragging ? 'lightgreen' : 'grey',
    ...draggableStyle,
});

class DraggableImage extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        let { item, index } = this.props;
        return (
            <Draggable key={item.id} draggableId={item.id} index={index}>
                {(provided, snapshot) => (
                    <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                        style={getItemStyle(
                            snapshot.isDragging,
                            provided.draggableProps.style
                        )}
                    >
                        <div className="row">
                            <img height="100" alt="images" width="100" src={item.src} />
                        </div>

                        <div className="row">
                            {item.content}
                        </div>

                    </div>
                )}
            </Draggable>
        )
    }

}

export default DraggableImage;