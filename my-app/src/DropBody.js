import React, { Component } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import DraggableImage from "./DraggableImage";

const grid = 8;
const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    display: 'flex',
    padding: grid,
    overflow: 'auto',
});

class DropBody extends Component {
    render() {
        let { items } = this.props;
        return (
            <Droppable droppableId="droppable" direction="horizontal">
                {(provided, snapshot) => (
                    <div
                        ref={provided.innerRef}
                        style={getListStyle(snapshot.isDraggingOver)}
                        {...provided.droppableProps}
                    >
                        {
                            items.map((item, index) => (
                                <DraggableImage item={item} index={index} />
                            ))
                        }

                        {provided.placeholder}
                    </div>
                )}
            </Droppable>
        );
    }
}

export default DropBody;